---
layout: page
menu: contribute
title: "Contribute"
---

# Contributing to Question2Answer

Development of this modified version of Question2Answer takes place through [GitLab](https://gitlab.com/jairlopez/question2answer). Bug reports and pull requests are encouraged, provided they follow these guidelines.


## Bug reports (issues)

If you find a bug (error) with Question2Answer, please [submit an issue here][Issues]. Be as descriptive as possible: include exactly what you did to make the bug appear, what you expect to happen, and what happened instead. Also include your PHP version and MySQL version. Remember to check for similar issues already reported.

If you think you've found a security issue, you can responsibly disclose it to us by [email](mailto:jair_lopez4321@hotmail.com).

Note that general troubleshooting issues such as installation or how to use a feature should continue to be asked on the [Question2Answer Q&A][QA].


## Pull requests

If you have found the cause of the bug in the Q2A code, you can submit the patch back to the Q2A repository. Create a fork of the repo, make the changes in your fork, then submit a pull request. Bug fix pull requests must be targeted to the `bugfix` branch. PRs for new features or large code changes must be made to the `dev` branch.

If you wish to implement a feature, you should start a discussion on the [Question2Answer Q&A][QA] first. We welcome all ideas but they may not be appropriate for the Q2A core. Consider whether your idea could be developed as a plugin.


[QA]: https://www.question2answer.org/qa/
[Issues]: https://gitlab.com/jairlopez/question2answer/issues/new
