---
layout: page
menu: addons
title: "Question2Answer - Add-ons - Clients"
---

# Question2Answer is being extended by the community.

This page links to add-ons created by Question2Answer users. These are not endorsed for quality or suitability, but we hope they are useful. If you have something to contribute, please [submit an issue](https://gitlab.com/jairlopez/question2answer-doc/-/issues/new) or [send a merge request](https://gitlab.com/jairlopez/question2answer-doc/-/merge_requests) for updating this documentation—your help is much appreciated!


## Clients

[QtoA-Mobile](https://appsmata.github.io/QtoA-Mobile/) by [Jacksiro254](https://question2answer.org/qa/user/Jacksiro254) is a new mobile app client for Question2Answer sites made using [Flutter 2.0](https://flutter.io) framework which means you can publish to either Android or iOS which uses qa-api, a simple Q2A custom API. To use QtoA-Mobile, you need to build **qa-mobile** flutter app for your site using Android Studio / Visual Studio Code as per your needs then clone **qa-api** to the root directory of your Q2A site.

Older Android clients include: [Q2A client for Android](https://github.com/NoahY/q2android) by [NoahY](http://www.question2answer.org/qa/user/NoahY) and [Q2A Android client](https://github.com/arjunsuresh/qaoverflow) by [Arjun](http://gateoverflow.in/user/Arjun/). Both require the [XML-RPC](https://github.com/arjunsuresh/q2a-xml-rpc) plugin to be installed on the Q2A site you're using with permission given to users in Admin page.
