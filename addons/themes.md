---
layout: page
menu: addons
title: "Question2Answer - Add-ons - Themes"
---

# Question2Answer is being extended by the community.

This page links to add-ons created by Question2Answer users. These are not endorsed for quality or suitability, but we hope they are useful. If you have something to contribute, please [submit an issue](https://gitlab.com/jairlopez/question2answer-doc/-/issues/new) or [send a merge request](https://gitlab.com/jairlopez/question2answer-doc/-/merge_requests) for updating this documentation—your help is much appreciated!

To install a theme, place its directory in Q2A's `qa-theme` directory, then open the General section of the Admin panel and choose the theme from the menu provided. **Many good themes can be found at [Q2A service providers](../services/)**. Below is a list of other recently updated themes:

You can also [create your own themes](../../themes/) using CSS only, or (optionally) [modify the HTML code](../../themes/#advanced).


## Free Themes

- [Blog Theme](https://github.com/ostack/qa-ostack-blog-theme) by [ZhaoGuangyue](https://www.linkedin.com/in/%E5%85%89%E8%B7%83-%E8%B5%B5-b58234146) based on SnowFlat. [Demo](https://www.ostack.cn). This theme can show pictures and content in question lists, to make the Q2A site look like a blog.
- [Brown-Clean](https://www.question2answer.org/qa/24972/new-free-theme-launched-brown-clean) by [Abhinav](https://www.question2answer.org/qa/user/abhik21).
- [Default Right-To-Left](https://www.question2answer.org/third-party/question2answer-theme-Default-R2L.zip) (based on Q2A Default theme) by [Towhid Nategheian](http://TowhidN.com/) @ [AskWeb].
- [Donut](https://github.com/amiyasahu/Donut) (responsive) by [Amiya Sahu](http://amiyasahu.com/).
- [Ejemplo de](http://www.ejemplode.com/q2a.zip) by Mauricio @ [Ejemplo de](http://www.ejemplode.com/preguntas/).
- [Esteem Theme](https://github.com/q2a-projects/Q2A-Esteem-Theme) (responsive) by [Towhidn](https://github.com/q2a-projects).
- [FatSnowFlat](https://github.com/ostack/qa-FatSnowFlat-theme) by [ZhaoGuangyue](https://www.linkedin.com/in/%E5%85%89%E8%B7%83-%E8%B5%B5-b58234146) based on SnowFlat. [Demo](https://www.ostack.cn). This theme can show pictures and content in question lists.
- [Google-style Mobile Theme](https://github.com/NoahY/q2a-google-mobile-theme) by [NoahY](https://www.question2answer.org/qa/user/NoahY).
- [IdeaBox Theme](https://github.com/q2a-projects/Q2A-IdeaBox-Theme) (responsive) by [Towhidn](https://github.com/q2a-projects).
- [Mayro](https://github.com/MominRaza/Mayro) (A Google Material Design Theme) by [Momin Raza](https://github.com/MominRaza).
- [Metro Theme](https://github.com/Towhidn/Q2A-Metro-Theme) by [Towhidn](https://github.com/q2a-projects).
- [Minimalist Answer](https://www.question2answer.org/qa/30250/theme-minimalist-answer-light-weight-social-enabled-theme) by [Digitizor Media](http://www.digitizormedia.com/).
- [Mobile Theme](https://github.com/NoahY/q2a-mobile-theme) by [NoahY](https://www.question2answer.org/qa/user/NoahY). Before Q2A 1.5, use the [Theme Switcher Plugin](https://github.com/NoahY/q2a-theme-switcher) to automatically switch themes for mobile users.
- [Parchment Theme](https://github.com/NoahY/q2a-parchment-theme) by [NoahY](https://www.question2answer.org/qa/user/NoahY).
- [Sharp](https://github.com/q2a-projects/Q2A-Sharp-Theme) by [Towhidn](https://github.com/q2a-projects).
- [Studio](https://github.com/q2a-projects/Q2A-Studio-Theme) by [Towhidn](https://github.com/q2a-projects).
- [Stylish](https://github.com/q2a-projects/Q2A-Stylish-Theme) (responsive) by [Towhidn](https://github.com/q2a-projects).
- [Support Theme](https://github.com/q2a-projects/Q2A-Support-Theme) by [Towhidn](https://github.com/q2a-projects).
- [Twenty Eleven](http://devmx.de/en/themes/twentyeleven-fur-q2a) by [Maximilian Narr](http://devmx.de/). For use with the WordPress theme of the same name.
- [TwentyTwelve](https://github.com/q2a-projects/Q2A-TwentyTwelve) by [Towhidn](https://github.com/q2a-projects). Based on WordPress default theme, TwentyTwelve.
- [Twitter Bootstrap](https://github.com/harshjv/q2a-bootstrap) by [Harsh J. Vakharia](https://twitter.com/harshjv). For Q2A 1.5.3 only, since it heavily modifies the core to work with [Bootstrap](https://github.com/twitter/bootstrap).


## Premium Themes

- [AVEN Theme](https://heliochun.github.io/shop/aven/) by [Hélio Chun](https://heliochun.github.io). AVEN is a carefully thought Material Design theme.
- [Frapuchino Theme](https://heliochun.github.io/shop/frapuchino/) by [Hélio Chun](https://heliochun.github.io). Frapuchino is a carefully thought Material Design theme.
- [Legacy Theme](https://heliochun.github.io/shop/legacy/) by [Shop](https://heliochun.github.io/shop/). Legacy is a carefully thought Material Design theme for the open source Question2Answer platform with inbuilt support for Q2A Blog Tool and many other plugins.
- [Mayro Pro](https://github.com/MominRaza/assets/blob/main/MayroPro.md) (A Google Material Design Theme with Dark Mode, RTL, PWA Support) by [Momin Raza](https://github.com/MominRaza).
- [Muffin Theme](https://heliochun.github.io/shop/muffin/) by [Shop](https://heliochun.github.io/shop/). Muffin is a carefully thought Material Design theme for the open source Question2Answer platform.
