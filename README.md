# Question2Answer documentation

This repository holds the documentation for a modified version of
[Question2Answer](https://www.question2answer.org/) platform at
[jairlopez.gitlab.io/question2answer-doc](https://jairlopez.gitlab.io/question2answer-doc/)
and generates it live using GitLab Pages. Everyone is welcome to contribute
improvements.

## Rationale for this repository

* Give our contribution to the Question2Answer community

* Allow Question2Answer's official owners and mantainers to pick the
  modifications they consider apropriate to merge into Question2Answer itself, without
  overloading them

* Offer an up-to-date platform and documentation to users, developers,
  designers, and experts

## How to Contribute

This site is built with the Jekyll static site creator, and hosted on GitLab
Pages. See [Contributing to the Q2A
docs](https://jairlopez.gitlab.io/question2answer-doc/contribute/docs/) for details on
installing and running Jekyll.

The [Question2Answer Q&A](https://www.question2answer.org/qa/) is also used for
discussion and help with Question2Answer as a whole.
